package apvereda.airportcodes;

import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.*;
import scala.Tuple2;

import java.util.Arrays;

/**
 * @author Alejandro Perez Vereda
 *
 */
public class AirportAnalysis
{
    public static void main( String[] args )
    {
        // STEP 1: argument checking
        if (args.length == 0) {
            throw new RuntimeException("The number of args is 0. Usage: "
                    + "SparkTwitterTT fileOrDirectoryName") ;
        }

        // STEP 2: create a SparkConf object
        SparkConf conf = new SparkConf().setAppName("AirportAnalysis") ;

        // STEP 3: create a Java Streaming Spark context
        JavaStreamingContext streamingContext = new JavaStreamingContext(conf, Durations.seconds(5));

        // STEP 4: read the lines from the file(s)
        JavaDStream<String> lines = streamingContext.textFileStream(args[0]);

        JavaPairDStream<String,Integer> airports = lines.filter(s -> Arrays.asList(s.split(",")).get(7).contains("ES"))
                .map(s -> Arrays.asList(s.split(",")).get(1))
                .mapToPair(s -> new Tuple2<>(s, 1))
                .reduceByKeyAndWindow((a, b) -> a + b, Durations.seconds(20), Durations.seconds(10));

        airports.print();

        streamingContext.start();
        try {
            streamingContext.awaitTermination();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
